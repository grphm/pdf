<?php
namespace APPLICATION_HOME\Providers;

use APPLICATION_HOME\Http\Controllers\PDF;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    protected $defer = FALSE;

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerConfig('application::config', 'Config/application.php');
        $this->registerSettings('application::settings', 'Config/settings.php');
        $this->registerActions('application::actions', 'Config/actions.php');
        $this->registerSystemMenu('application::menu', 'Config/menu.php');
        $defines = $this->app['config']->get('dompdf.defines') ? : [];
        foreach($defines as $key => $value):
            $this->define($key, $value);
        endforeach;
        $this->define("DOMPDF_ENABLE_REMOTE", TRUE);
        $this->define("DOMPDF_ENABLE_AUTOLOAD", FALSE);
        $this->define("DOMPDF_CHROOT", realpath(base_path()));
        $this->define("DOMPDF_LOG_OUTPUT_FILE", storage_path('logs/dompdf.html'));
        $config_file = $this->app['config']->get('dompdf.config_file') ? : base_path('vendor/dompdf/dompdf/dompdf_config.inc.php');
        if(file_exists($config_file)):
            require_once $config_file;
        endif;
    }

    public function register() {

        $configPath = __DIR__.'/../Config/dompdf.php';
        $this->mergeConfigFrom($configPath, 'dompdf');
        $this->app->bind('dompdf', function($app) {

            $dompdf = new \DOMPDF();
            $dompdf->set_base_path(realpath(base_path('public')));
            return $dompdf;
        });
        $this->app->alias('dompdf', 'DOMPDF');
        $this->app->bind('dompdf.wrapper', function($app) {

            return new PDF($app['dompdf'], $app['config'], $app['files'], $app['view']);
        });
    }

    public function provides() {

        return ['dompdf', 'dompdf.wrapper'];
    }

    protected function define($name, $value) {

        if(!defined($name)):
            define($name, $value);
        endif;
    }
}