<?php
namespace APPLICATION_HOME\Http\Controllers;

use DOMPDF;
use Exception;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\Factory as ViewFactory;
use Illuminate\Contracts\Config\Repository as ConfigRepository;
use Illuminate\Http\Response;

class PDF extends ModuleController {

    protected $dompdf;
    protected $config;
    protected $files;
    protected $view;
    protected $rendered = FALSE;
    protected $orientation;
    protected $paper;
    protected $showWarnings;
    protected $public_path;

    public function __construct(DOMPDF $dompdf, ConfigRepository $config, Filesystem $files, ViewFactory $view) {

        $this->dompdf = $dompdf;
        $this->config = $config;
        $this->files = $files;
        $this->view = $view;
        $this->showWarnings = $this->config->get('dompdf.show_warnings', FALSE);
        if($this->config->has('dompdf.paper')):
            $this->paper = $this->config->get('dompdf.paper');
        else:
            $this->paper = DOMPDF_DEFAULT_PAPER_SIZE;
        endif;
        $this->orientation = $this->config->get('dompdf.orientation') ? : 'portrait';
    }

    public function getDomPDF() {

        return $this->dompdf;
    }

    public function setPaper($paper, $orientation = NULL) {

        $this->paper = $paper;
        if($orientation):
            $this->orientation = $orientation;
        endif;
        return $this;
    }

    public function setOrientation($orientation) {

        $this->orientation = $orientation;
        return $this;
    }

    public function setWarnings($warnings) {

        $this->showWarnings = $warnings;
        return $this;
    }

    public function loadHTML($string, $encoding = NULL) {

        $string = $this->convertEntities($string);
        $this->dompdf->load_html($string, $encoding);
        $this->rendered = FALSE;
        return $this;
    }

    public function loadFile($file) {

        $this->dompdf->load_html_file($file);
        $this->rendered = FALSE;
        return $this;
    }

    public function loadView($view, $data = array(), $mergeData = array(), $encoding = NULL) {

        $html = $this->view->make($view, $data, $mergeData)->render();
        return $this->loadHTML($html, $encoding);
    }

    public function output() {

        if(!$this->rendered):
            $this->render();
        endif;
        return $this->dompdf->output();
    }

    public function save($filename) {

        $this->files->put($filename, $this->output());
        return $this;
    }

    public function download($filename = 'document.pdf') {

        $output = $this->output();
        return new Response($output, 200, ['Content-Type' => 'application/pdf', 'Content-Disposition' => 'attachment; filename="'.$filename.'"']);
    }

    public function stream($filename = 'document.pdf') {

        $output = $this->output();
        return new Response($output, 200, ['Content-Type' => 'application/pdf', 'Content-Disposition' => 'inline; filename="'.$filename.'"']);
    }

    protected function render() {

        if(!$this->dompdf):
            throw new Exception('DOMPDF not created yet');
        endif;
        $this->dompdf->set_paper($this->paper, $this->orientation);
        $this->dompdf->render();
        if($this->showWarnings):
            global $_dompdf_warnings;
            if(count($_dompdf_warnings)):
                $warnings = '';
                foreach($_dompdf_warnings as $msg):
                    $warnings .= $msg."\n";
                endforeach;
                if(!empty($warnings)):
                    throw new Exception($warnings);
                endif;
            endif;
        endif;
        $this->rendered = TRUE;
    }

    protected function convertEntities($subject) {

        $entities = ['€' => '&#0128;'];
        foreach($entities as $search => $replace):
            $subject = str_replace($search, $replace, $subject);
        endforeach;
        return $subject;
    }
}