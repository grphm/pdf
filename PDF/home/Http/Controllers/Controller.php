<?php
namespace APPLICATION_HOME\Http\Controllers;

class Controller extends ModuleController {

    public function index() {

        $request = new \Request();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML(view('site_views::pdf', ['data' => $request::all()])->render());
        $filePath = storage_path('app/'.md5(time().rand(0, 1000)).'.pdf');
        $pdf->setPaper('A4', 'landscape')->setWarnings(FALSE)->save($filePath);
    }
}